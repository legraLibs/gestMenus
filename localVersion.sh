#!/bin/sh
# Ce fichier doit etre copier dans le repertoire ./ (racine) du projet et modifier
#v2.1.0

#######################################
# updateModele()                      #
# met les lib a jours (par linkage)   #
#######################################
updateModele(){
	echo "$couleurINFO # - localVersion.sh:updateModele() - #$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then

		# -- gestionnaire de versions -- #
		lib='gitVersion';
		mkdir -p ./styles
		mkdir -p ./scripts/;
		#version dev
		libV="$lib.sh";echo "$couleurINFO lib:$libV $couleurNORMAL";link /www/git/bash/$lib/scripts/$libV ./scripts/$libV;
		#version fixe
		#libV="$lib-v2.0.1.sh";echo "$couleurINFO lib:$libV $couleurNORMAL"; link /www/git/bash/$lib/versions/scripts/$libV ./scripts/$libV;

		# -- librairies  -- #
		lib='gestLib';
		mkdir -p ./lib/legral/php/$lib;
		#version dev
		libV="$lib.php";echo "$couleurINFO lib:$libV $couleurNORMAL"; link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		libV="$lib.js"; echo "$couleurINFO lib:$libV $couleurNORMAL"; link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		#version fixe
		libV="$lib-v2.0.0.php";echo "$couleurINFO lib:$libV $couleurNORMAL";cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;
		libV="$lib-v2.0.0.js";echo "$couleurINFO lib:$lib $couleurNORMAL";  cp /www/git/intersites/lib/legral/php/$lib/versions/$libV ./lib/legral/php/$lib/$libV;
		echo "$couleurWARN metter a jours localVersion.sh $couleurNORMAL";
		#styles
		libV="$lib.css";echo "$couleurINFO css:$libV $couleurNORMAL";link /www/git/intersites/lib/legral/php/$lib/styles/$libV ./lib/legral/php/$lib/$libV;

		lib='gestMenus';
		mkdir -p ./lib/legral/php/$lib;
		#libV="$lib.php";echo "$couleurINFO lib:$libV $couleurNORMAL"; link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		#libV="$lib.css";echo "$couleurINFO css:$libV $couleurNORMAL";link /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;

		#libV="menuStylisee.php";echo "$couleurINFO lib:$libV $couleurNORMAL";cp /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;
		#libV="menuStylisee.css";echo "$couleurINFO lib:$libV $couleurNORMAL";cp /www/git/intersites/lib/legral/php/$lib/$libV ./lib/legral/php/$lib/$libV;

		# -- styles communs -- #
		css='';
		cssV=$css'knacss.css';echo "$couleurINFO css:$cssV $couleurNORMAL";link /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;

		css='';
		cssV=$css'html4.css';echo "$couleurINFO css:$cssV $couleurNORMAL";link /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;

		css='';
		cssV=$css'intersites.css';echo "$couleurINFO css:$cssV $couleurNORMAL";link /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;

		css='notes';
		cssV=$css'Relatives.css';echo "$couleurINFO css:$cssV $couleurNORMAL";link /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;

		css='tutoriels';
		cssV=$css'Relatif.css';echo "$couleurINFO css:$cssV $couleurNORMAL";link /www/git/gitModele/styles/$css/$cssV ./styles/$cssV;

		# -- menus communes -- #
		echo "";
		echo "$couleurINFO pages communes $couleurNORMAL";
		mkdir -p ./menus/
		f='menus/menus-systeme.php';echo "$couleurINFO linkage de $f $couleurNORMAL";link /www/git/gitModele/$f $f;
		f='pagesLocales/typo';echo "$couleurINFO ln -s de $f $couleurNORMAL";ln -s /www/git/gitModele/$f $f;

		# -- pages communes -- #
		echo "";
		echo "$couleurINFO pages communes $couleurNORMAL";
		mkdir -p ./pagesLocales/_site/
		f='_site/about.php';echo "$couleurINFO linkage de $f $couleurNORMAL";link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;
		f='_site/credits.php';echo "$couleurINFO linkage de $f $couleurNORMAL";link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;
		f='_site/inspect.php';echo "$couleurINFO linkage de $f $couleurNORMAL";link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;
		f='_site/plans.php';echo "$couleurINFO linkage de $f $couleurNORMAL";link /www/git/gitModele/pagesLocales/$f ./pagesLocales/$f;


		# -- fichiers communs -- #
		echo "$couleurINFO fichiers communs $couleurNORMAL";
		link /www/git/gitModele/piwik.php ./piwik.php;

		# -- Changement des droits  -- #
		echo "$couleurINFO Changement des droits $couleurNORMAL";
		chmod -R 755 ./scripts/
		chmod -R 755 ./styles/
		chmod -R 755 ./lib/
		#chmod -R 755 ./lib/legral/php
		#chmod -R 755 ./lib/legral/js
	fi
}

#########################################################
# localConcta()                                         #
# concatenne les fichiers css dans ./styles/styles.css  #
# concatenne les fichiers js  dans ./locales/scripts.js #
#########################################################
localConcat() {
        # - concatenation des fichiers css - #
	echo "$couleurINFO # - concatenation des fichiers css dans ./styles/styles.css - #$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		catCSS ./styles/knacss.css
		catCSS ./styles/html4.css

		catCSS ./styles/intersites.css

		catCSS ./lib/legral/php/gestLib/gestLib.css
	
		#catCSS ./lib/legral/php/menuStylisee/menuStylisee.css
		#catCSS ./lib/legral/php/gestMenus/menuStylisee.css
		catCSS ./gestMenus.css
		catCSS ./menuStylisee.css

		#catCSS ./lib/legral/php/gestMenus/gestMenus.css
		catCSS ./gestMenus.css

		catCSS ./styles/notesRelatives.css
		f='./styles/tutorielsRelatif.css';cmd="catCSS $f";	echo "$couleurINFO $cmd $couleurNORMAL";eval $cmd;

	fi

        # - concatenation des fichiers js - #
	echo "";
        echo "$couleurINFO # - concatenation des fichiers js dans ./locales/scripts.js  - #$couleurNORMAL";
	echo "$couleurWARN Pas de lib js à concatener $couleurNORMAL";return 1;	# a commenter une fois parametré
	if [ $isSimulation -eq 0 ];then
	echo "$couleurWARN Pas de lib js à concatener $couleurNORMAL";return 1;	# a commenter une fois parametré
		#f='/www/git/intersites/lib/tiers/js/jquery/jquery-2.1.1.min.js';	echo "$couleurINFO catJS de $f $couleurNORMAL";catJSS $f
	fi

}

##################################################
# localSave()                                    #
# copie un fichier en le suffixant de la version #
##################################################
localSave() {

	echo "$couleurINFO versionning des fichiers locaux$couleurNORMAL";
	echo "$couleurWARN Rien à versionner $couleurNORMAL";return 1;	# a commenter une fois parametré

	if [ $isSimulation -eq 0 ];then
		echo "$couleurWARN Rien à versionner $couleurNORMAL";return 1;	# a commenter une fois parametré

		# - fichier a copier dans versions (en ajoutant la version dans le mon du fichier) - #
		versionSave ./gestMenus php
		versionSave ./gestMenus css

		# fichier js exemple:
		#versionSave script js
	fi
}
#######################################
# localStatification()                #
# télécharge et rend statique un site #
#######################################
localStatification(){
	echo "$couleurINFO # - localVersion.sh:localStatification() - #$couleurNORMAL";
	url="http://127.0.0.1/git/intersites/lib/legral/php/gestMenus/";

	echo "telechargement avec rendu statique d'un site de $url";
	#echo "$couleurWARN Rien à statifier $couleurNORMAL";return 1;	# a commenter une fois parametré

	if [ $isSimulation -eq 0 ];then

		#echo "$couleurINFO suppression et creation de ../statique/$couleurNORMAL";
		rm -R ./statique/;mkdir ./statique/;cd   ./statique/;

		echo "$couleurINFO téléchargement...$couleurWARN";

		#--no-verbose --quiet
		wget --quiet --tries=5 --continue --no-host-directories --html-extension --recursive --level=inf --convert-links --page-requisites --no-parent --restrict-file-names=windows --random-wait --no-check-certificate $url

		echo "$couleurINFO deplacer et nettoyer le chemin$couleurNORMAL";
		mv ./git/intersites/lib/legral/php/gestMenus/ ./

		# - decommenter pour activer la suppression apres verification - #
		rm -R ./git/

		echo "$couleurINFO la version statique se trouve: $url/statique $couleurNORMAL";
	fi
}


#######################################
# syncRemote()                        #
# synchronise le(s) serveurs distants #
#######################################
syncRemote(){
	#echo "$couleurWARN # - pas de Remote - #$couleurNORMAL";return 1; # a commenter une fois parametré
	urlDest='intersites/lib/legral/php/gestMenus';
	if [ $isSimulation -eq 0 ];then
		#echo "$couleurWARN # - pas de Remote - #$couleurNORMAL";return 1; # a commenter une fois parametré
		#echo "exemple:";
		lftp -u legral ftp://ftp.legral.fr -e "mirror -e -R  ./statique/gestMenus   /www/$urlDest ; quit"
	fi
	echo "$couleurINFO url: http://legral.fr/$urlDest $couleurNORMAL";
}

#################
# postGit()     #
# lancer en fin #
#################
postGit() {
	echo "$couleurINFO # - localVersion.sh:postGit() - #$couleurNORMAL";
	echo "$couleurWARN Pas de postGit $couleurNORMAL";return 1; # a commenter une fois parametré

	if [ $isSimulation -eq 0 ];then
		echo "$couleurWARN Pas de postGit $couleurNORMAL";return 1; # a commenter une fois parametré
	fi
}
