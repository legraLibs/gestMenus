#!/bin/sh
# - configuration - #
VERSION="v2.1.2";
noStatus=0;             # si 1: git status ne sera pas lancer
no0Pages=1;		# si 1: ne monte pas /www/0pages

# - variable interne - #
scriptPath=$0;           #chemin complet du script 
scriptRep=`dirname $0`;  #repertoire du script
#dirname="${1%${1##*/}}" # donne ?
localVersionPath="./localVersion.sh";
islocalVersionLoad=0;	# le fichjier localVersion existe t'il?
isSimulation=0;		# -n ($simulerArg)
simulerArg="-n";
template="";		# creation de rep et fichier (modele de projet)

isUpdateModele=0;	# exec updateModele dans ./localVersion.sh (link les libs,etc)
isMin=0;
isStatic=0;		# --static
isRemote=0;		# --remote
isCommit=0;
tag=0;			# tag du commit (version du programme sous la forme v0.0.0
commitFail=0;		# egale a 1 si le commit a echoue
isPush=0;
isStatus=0;		# faire un git status en fin d'operation ? (et afficher les tags)

debug=0;
verbosity=0;
verboseArg='-v';

# - chargement des codes couleurs - #
#. $scriptRep/legralcouleur.sh
################
# colorisation #
###############

BOLD=`tput smso`
NOBOLD=`tput rmso`

#NORMAL="\[\e[0m\]"
BLACK=`tput setaf 0`
#RED="\[\e[1;31m\]"
RED=`tput setaf 1`
GREEN=`tput setaf 2`
YELLOW=`tput setaf 3`
CYAN=`tput setaf 4`
MAGENTA=`tput setaf 5`
BLUE=`tput setaf 6`
WHITE=`tput setaf 7`

couleurNORMAL=$WHITE
couleurINFO=$BLUE
couleurCOM=$YELLOW
couleurWARN=$RED
couleurTITRE1=$GREEN
couleurTITRE2=$MAGENTA

export BOLD
export NOBOLD
export BLACK
export RED
export GREEN
export YELLOW
export CYAN
export MAGENTA
export BLUE
export WHITE

export couleurNORMAL
export couleurINFO
export couleurCOM
export couleurWARN
export couleurTITRE1
export couleurTITRE2

echo "$couleurINFO$repScript gitVersion $VERSION$couleurNORMAL";
echo "gitVersion --help";


#########
# usage #
#########
usage () {
	echo " ./scripts/gitVersion.sh$couleurINFO options$couleurNORMAL";
	echo " $simulerArg | --simule simule les actions";
	echo "";
	echo " --template=$couleurINFO template$couleurNORMAL: intialise les fichiers selon le modele:";
	echo "  sh: le projet est un script shell";
	echo "  web: le projet est un site web";
	echo "";
	echo " --min: concat et minimise les js et css (defini dans ./localVersion.sh)";
	echo " --static: statifie un site avec wget (lance ./scripts/localStatic.sh)"
	echo " --remote (lance syncRemote() dans localVersion) ";
	echo "";
	echo " --commit=$couleurINFO tag$couleurNORMAL:commit les changements et les tag avec version (active --min)";
	echo " --push: push les commits et les tag ";
	echo "";
	echo " --remote (lance syncRemote() dans localVersion) ";
	echo " --status: faire un git status en fin d'operation (et afficher les tags)";
	echo "$couleurINFO exemples:";
	echo "./scripts/gitVersion.sh --template=modele ";
	echo "./scripts/gitVersion.sh --updateModele : execute updateModele dans localVersion.sh";
	echo "./scripts/gitVersion.sh --simule --commit=tag --push --status";
	echo "Toutes les opérations dans l'ordre:";
	echo "./scripts/gitVersion.sh --simule --updateModele --min --commit=tag --push --status --static --remote  $couleurNORMAL";
	exit
	}	

##################
# analyse du psp #
#$0	Contient le nom du script tel qu'il a été invoqué
#$*	L'ensembles des paramètres sous la forme d'un seul argument
#$@	L'ensemble des arguments, un argument par paramètre
#$#	Le nombre de paramètres passés au script
#$?	Le code retour de la dernière commande
#$$	Le PID su shell qui exécute le script
#$!	Le PID du dernier processus lancé en arrière-plan
#
# -v:: --verbose:: argument optionnel
# -V --version
# --in: les cibles (rep/fichiers) provienne de l'entree standart
#
#TEMP=`getopt \
#     --options ab:c:: \
#     --long a-long,b-long:,c-long:: \
#     -n 'example.bash' \
#      -- "$@"
#echo "Remaining arguments:"
#for arg do echo '--> '"\`$arg'" ; done
#
##################
TEMP=`getopt \
     --options v::dVhn \
     --long verbose::,debug,version,help,status,template::,updateModele,min,static,remote,commit::,tag::,push \
     -- "$@"`

# Note the quotes around `$TEMP': they are essential!
eval set -- "$TEMP"
#if [ $? -eq 0 ] ; then echo "options requise. Sortie" >&2 ; exit 1 ; fi
#echo "$couleurWARN nb: $# $couleurNORMAL";
if [ $# -eq 1 ] ; then  usage; exit 1 ; fi #pas de parametre


while true ; do
        case "$1" in
                -h|--help)usage; exit 0; shift ;;
                #-v|--verbose) echo "Option b, argument \`$2'" ; shift 2 ;;
                -V|--version) echo "$VERSION"; exit 0; shift ;;
                -v|--verbose)
                        case "$2" in
                                "") verbosity=1; shift 2 ;;
                                *)  #echo "Option c, argument \`$2'" ;
                                        verbosity=$2; shift 2;;
                        esac ;;
		-d|--debug)
                        case "$2" in
                                "") debug=1;  shift 2 ;;
                                *)  debug=$2; shift 2 ;;
                        esac ;;

		-n)isSimulation=1; shift ;;
		--noStatus) noStatus=1; shift ;;

		--updateModele) isUpdateModele=1; shift ;;
		--template)
			case "$2" in
				"") echo "$couleurWARN nom du modele obligatoire$couleurNORMAL"; exit;  shift 2 ;;
				*) template="$2";  shift 2  ;;
			esac ;;


		--min) isMin=1; shift ;;
		--static) isStatic=1; shift ;;

		--commit)
			case "$2" in
				"") echo "$couleurWARN la version(tag) du commit est obligatoire $couleurINFO gitVersion --commit=tag$couleurNORMAL "; exit ; shift 2;;
				*) isCommit=1;tag="$2";
					isMin=1;	# active la minimisation
					shift 2  ;;
			esac ;;

		--push) isPush=1; shift ;;

		--status) isStatus=1; shift ;;

		--remote) isRemote=1; shift ;;


		--) shift ; break ;;
                *) echo "option $1 non reconnu"; shift;  exit 1 ;; # sans exit: si la derniere option est inconnu -> boucle sans fin
        esac
done


	
#############
# functions #
#############
# -- creation du repo git si non existant -- #
createRepoGit(){
	echo "";
	rep="./.git";
	echo  "$couleurINFO\creation du repo git$couleurNORMAL";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation d'un repo initial$couleurNORMAL";
		if [ $isSimulation -eq 0 ];then
			git init;
		fi
	        echo "$couleurWARN taper 'git push --set-upstream origin master' pour le positionner sur master $couleurNORMAL";
        fi
}


########################
# creation du template #
########################
createTemplate(){
	case "$template" in
		web) createTemplateWeb ;;
                shell) createTemplateShell ;;
        esac
}


createTemplateCommun(){
	echo "";
	echo "$couleurINFO creation du template: Commun$couleurNORMAL";
	rep="./versions";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	        mkdir $rep
	        fi

	# -- creation du repertoire des documents (generales) locales -- #
	rep="./locales";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	        mkdir $rep
	        fi
}


createTemplateShell(){
	createTemplateCommun;
	echo "";
	echo "$couleurINFO creation du template: Shell$couleurNORMAL";
	echo "";
}

createTemplateWeb(){
	createTemplateCommun;
	echo "";
	echo "$couleurINFO creation du template: Web$couleurNORMAL";

	# -- creation du repertoire des styles -- #
	rep="./styles";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	        mkdir $rep
	        fi

	# -- creation du repertoire des pages locales -- #
	rep="./pagesLocales";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	        mkdir $rep
	        echo  "pages specifique a ce projet. Les pages communes sont dans ./pages/ (repertoires montes depuis /www/0pages/" > $rep/readme
	        fi

	# -- creation du repertoire static (recoit les versions staitc de sites -- #
	rep="./statique";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	        mkdir $rep
	        fi


	# -- creation du repertoire des pages communes et montage -- #
	rep="./pages";
	if [ -d "$rep" ];
	then
	        echo "$couleurINFO repertoire $rep existant$couleurNORMAL";
	else
	        echo "$couleurWARN repertoire $rep NON existant -> creation$couleurNORMAL";
	        mkdir $rep
	        fi

	if [ "$no0Pages" -eq 0 ];then
	        echo "montage de /www/0pages dans $rep";
	        fichier="$rep/0pages";
	        if [ -f "$fichier" ];
	        then
	                echo "$couleurINFO presence de $fichier detecte: le repertoire est considere comme deja monte";
	        else
	                echo "si la fonction ne marche pas modifier /etc/sudoers$couleurWARN";
	                sudo mount --read-only --bind /www/0pages $rep
	                echo "$couleurNORMAL";
	                fi
	else
	        echo "$couleurINFO 0Pages non montes suivant configuration$couleurNORMAL";
	        fi

	# - creation/remise a zero des fichiers locaux - #
	echo "$couleurINFO creation/remise a zero des fichiers locales $couleurNORMAL";
	echo "/*! fichier generer automatiquement et compresser avec yuicompressor */" > ./styles/styles.css
	echo  "/*! fichier generer automatiquement et compresser avec yuicompressor */" > ./locales/scripts.js
	echo "";

	# - copie des fichiers non linker (voir localVersion pour le linkage) - #
	echo "$couleurINFO copie des fichiers  non linker (voir localVersion pour le linkage) $couleurNORMAL";
	f='index.php';echo "copie de $f";cp /www/git/gitModele/$f ./$f;

	# -- Changement des droits  -- #		echo "$couleurINFO Changement des droits $couleurNORMAL";
	echo "$couleurINFO Changement des droits $couleurNORMAL";
	chmod -R 755 ./index.php


}


##################################################
# - minimise les fichiers js ou css (selon $2) - #
# minSuf: suffixe de la minification             #
#               N EST PAS UTILISE!!!!            #          
##################################################
#versionMinimise() {
#	echo "";
#	fn=$1;
#	fe=".$2";
#	minSuf=".min";
#	fm="$fn$minSuf-$tag$fe";
#        echo "compression de  $fn$fe vers $fm$fe";
#	java -jar ${scriptRep}/scripts/yuicompressor-2.4.8.jar $fn$fe -o ./versions/$fm
#}

############################################
############################################
# - functions utiliser par localesSave() - #
############################################
############################################

# - concatenation de fichier - #
catJS(){
	echo "" >> ./locales/scripts.js
	echo "/*! - $1 - */" >> ./locales/scripts.js
	#echo "$couleurWARN";
        cat "$1"            >> ./locales/scripts.js
	#echo "$couleurNORMAL";
}

catCSS(){
	echo "" >> ./styles/styles.css
	echo "/*! - $1 - */" >> ./styles/styles.css
	#echo "$couleurWARN";
        cat "$1"            >> ./styles/styles.css
	#echo "$couleurNORMAL";
}

#####################################################################
# - creer une copie d'un js|css en ajoutant la version en suffixe - #
# %1:fn:fichier nom
# %2:fe: fichier extention (sans le point)
# (calc)fv: nom du fichier avec la version en suffixe
# utilisser par localSave()                                         #
#####################################################################
versionSave() {
	fn=$1;
	fe=".$2";	# ajout du point 
        fv="$fn-$tag";
	echo "creation de la copie $fn$fe vers ./versions/$fv$fe";
        cp $fn$fe ./versions/$fv$fe
	if [ "$fe" = "sh" ];then
		echo "le fichier est un script -> +x"
		chmod +x ./versions/$fv$fe
	fi
}


##########################################
# callUpdateModele                          #
# execute updateModele() ds ./localVersion.sh #
##########################################
callUpdateModele(){
	if [ $isUpdateModele -eq 1 ];then
		echo "";
		echo "$couleurINFO # - Mise a jours du modele - #$couleurNORMAL";
		updateModele;
		echo "$couleurWARN metter a jours localVersion.sh $couleurNORMAL";
		echo "$couleurINFO Etape suivante: ./scripts/gitVersion.sh --min  $couleurNORMAL";
	fi
}

#########################################################
# - generere les fichiers contennue dans ./locales/ ) - #
# - generation de script.js et styles.css -             #
#########################################################
localGenMin(){
	# - chargement config local - #
	if [ $isMin -eq 1 ];then
	if [ $islocalVersionLoad -eq 1 ];then
		echo "";
		mkdir ./locales
		# - creation de ./styles/styles.css ./locales/scripts.js - #
		echo "$couleurINFO creation de style.css et de ./locales/scripts.js.$couleurNORMAL";
		if [ $isSimulation -eq 0 ];then
			echo '@charset "utf-8";' > ./styles/styles.css; 
			echo '/*! ./locales/scripts.js*/' > ./locales/scripts.js
			fi

		# - concatenation des fichiers css et js - #
		echo "$couleurINFO localConcat: concatenation des fichiers css et js.$couleurNORMAL";
		if [ $isSimulation -eq 0 ];then localConcat; fi	# cette fonction est dans le fichier $localVersionPath 

		# - minimise scripts.js - # 
		# yuicompressor NE rajoute PAS automatiquement l'extention ".js|.css"

		echo "compression de ./locales/scripts.js .$couleurNORMAL";
		if [ $isSimulation -eq 0 ];then
			java -jar ${scriptRep}/yuicompressor-2.4.8.jar ./locales/scripts.js -o ./locales/scripts.min.js
		fi


		echo "compression de ./styles/styles.css .$couleurNORMAL";
		if [ $isSimulation -eq 0 ];then
			java -jar ${scriptRep}/yuicompressor-2.4.8.jar ./styles/styles.css -o ./styles/styles.min.css
			fi

		echo "$couleurINFO Etape suivante: ./scripts/gitVersion.sh --commit=v0.0.0  $couleurNORMAL";
		echo "$couleurINFO           ou #$couleurNORMAL";
		echo "$couleurINFO Etape suivante: ./scripts/gitVersion.sh --static $couleurNORMAL";

	fi #if [ $islocalVersionLoad -eq 1 ]then
	fi #if [ $isMin -eq 1 ] then
}



##########
# commit #
##########
commit(){
if [ $isCommit -eq 1 ];then 
	echo "";
	echo "$couleurINFO preparation du commit...";

	#localGenMin; #activer a l'appel de --commit

	if [ $islocalVersionLoad -eq 0 ];then echo "$couleurWARN pas de fichier $localVersionPath$couleurNORMAL"; fi

	if [ $islocalVersionLoad -eq 1 ];then
		# - creation des versions et copies dans ./versions - #	
		echo "copie des fichiers versionnees dans ./version";
		if [ $isSimulation -eq 0 ];then	 localSave; fi
	fi

	# - mise a jours du fichier contenant la version - #
	echo "$couleurINFO mise a jours du fichier lastVersion$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		echo "$tag" > ./lastVersion
	fi

	# - On add les fichiers - #
	echo "$couleurINFO git add .$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		git add .
	fi

	# - on commit  - #
	echo "$couleurINFO maintenant on commit (copier le texte issue de release)$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		git commit
	fi

	echo "$couleurINFO on tag le commit avec $couleurWARN$tag$couleurNORMAL";
	if [ $isSimulation -eq 0 ];then
		git tag $tag
	fi
	echo "$couleurINFO Etape suivante: ./scripts/gitVersion.sh --push  $couleurNORMAL";

	
fi #if [ $isCommit -eq 1 ];then 
}

########
# push #
########
push(){
if [ $isPush -eq 1 ];then
	echo "";
	if [ $isSimulation -eq 0 ];then
		echo "$couleurINFO on push les commits$couleurNORMAL";
		git push;
		echo "$couleurINFO on push les tags$couleurNORMAL";
		git push --tag
		echo "$couleurINFO Etape suivante: ./scripts/gitVersion.sh --static  $couleurNORMAL";
	else
		echo "$couleurWARN mode test: pas de push $couleurNORMAL";
		fi
fi #if [ $isPush -eq 1 ];then 
} 


#################################################
# callStatification                             #
# appel localStatification dans localVersion.sh #
################################################
callStatification (){
	if [ $isStatic -eq 1 ];then
		echo "";
		echo "$couleurINFO # - gitVersion.sh - callStatification - #$couleurNORMAL";
		localStatification;
		echo "$couleurINFO Etape suivante: ./scripts/gitVersion.sh --remote  $couleurNORMAL";
	fi
}

#####################################
# callSyncRemote                    #
# appel remote dans localVersion.sh #
#####################################
callSyncRemote(){
	if [ $isRemote -eq 1 ];then
		echo "$couleurINFO # - localVersion.sh:syncRemote() - #$couleurNORMAL";
		echo "$couleurINFO # - Penser a mettre a jours la version statique avec ./scripts/gitVersion.sh --static - #$couleurNORMAL";
		echo "$couleurINFO # - http://doc.ubuntu-fr.org/lftp - #$couleurNORMAL";
		echo "$couleurINFO # - configurer ~.netrc pour ne pas a avoir a fournir le passsword - #$couleurNORMAL";
		if [ $isSimulation -eq 0 ];then
			syncRemote;
		fi
	fi
}



######################
# fonction de sortie #
######################
sortie() {
	echo "--------------------------";
	if [ $isStatus -eq 1 ];then

		echo "$couleurINFO on affiche le status:$couleurNORMAL"
		git status;

		echo "$couleurINFO on affiche les tags$couleurNORMAL";
		git tag;
	fi
		
	# - postGit : appelle de la function postGit (defini dans localVersion) - #
	postGit;
	exit 0;	#on quitte le script
	}


########
# main #
########
#PROGVERSION=$1;

#if [ $# -eq 0 ];then
#	usage;
#fi

# - on se positionne sur la racine du projet - #
cd $scriptRep/../
echo "$couleurINFO repertoire courant: $couleurNORMAL";
pwd;

# - indique si c'est une simulation - #
if [ $isSimulation -eq 1 ];then
	echo "$couleurWARN simulation aucune action ne sera reelement faite$couleurNORMAL";
fi


# - creation du repo git si non existant - #
createRepoGit;

# - Creation des templates - #
createTemplate;

# - chargement de la configuration utilisateur (fichier: localVersion.sh) - #
if [ -f "$localVersionPath" ];
then
	echo "$couleurINFO Chargement de $localVersionPath $couleurNORMAL";
	. $localVersionPath;
	islocalVersionLoad=1;
else
	echo "$couleurWARN pas de fichier $localVersionPath$couleurNORMAL";
	islocalVersionLoad=0;
	fi

callUpdateModele;
localGenMin;
callStatification;
commit;
push;
callSyncRemote;
 

# - sortie; -#
#postGit;
sortie;
exit 0;	#on quitte le script

