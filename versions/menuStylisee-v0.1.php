<?php
/*!******************************************************************
fichier: menuStylisee.php
version : voir declaration gestLib
auteur : Pascal TOLEDO
date de creation: 2014.05.08
.date de modification 2014.07.12
source: http://www.legral.fr/intersites/
depend de:
	* aucune
description:
	* met en forme, par application d'une feuille de style, des menus (sous forme d'onglet ou autre)
*******************************************************************/
$gestLib->loadLib('menuStylisee',__FILE__,'0.1',"template de menu");
//      $gestLib->lib['menuStylisee']->setErr(LEGRALERR::DEBUG);

class menuStylisee_li
	{
	// - parametre obligatoire- //
	public $titre='';
	public $getHref='';   // get de type menu=page qui sert a trouver l'url (obligatoire) <a>

	// - parametre facultatif - //
	private $isVisible=1;   // visible par defaut
	private $varGet='';	// 

	public $inlineLI='';	// ajoute $inlineLI dans le code html de <li>
	public $inlineA='';	// ajoute $inlineLI dans le code html de <a>

	private $cssLI='';	// liste de class qui seront directement integre ds le code html de <li>
	private $cssSP='';	// idem pour le span
	private $cssA='';	// idem pour le <a> 
	
	private $title='';	// text d'info bulle sur <a>

	function __toString(){$out='<b>'. __CLASS__.'</b><ul>';foreach ($this as $key => $value){$out.="<li>$key: $value</li>";}return $out.'</ul>';}


	function __construct($titre,$getHref/*,$cssLI='',$cssA='',$inlineLI='',$inlineA=''*/){
		$this->titre=$titre;
		$this->getHref=$getHref;

		$this->varGet=$this->varGet?$this->varGet:$this->titre;

//		$this->cssLI=$cssLI;$this->inlineLI=$inlineLI;
//		$this->cssLI=$cssA ;$this->inlineA=$inlineA;
		}

	function getVarGet(){return $this->varGet;}     function setVarGet($vg){$this->varGet=$vg;}

	function getVisible(){return $this->isVisible;}	function setVisible($v=1){if($v==0 or v==1)$this->isVisible=$v;}

	function getCssLI(){return $this->cssLI;}       function setCssLI($css){$this->cssLI=$css;}function addCssLI($css){$this->cssLI.=" $css";}
	function getCssSP(){return $this->cssSP;}       function setCssSP($css){$this->cssSP=$css;}function addCssSP($css){$this->cssSP.=" $css";}
	function getCssA (){return $this->cssA; }       function setCssA ($css){$this->cssA =$css;}function addCssA ($css){$this->cssA .=" $css";}

	function setTitle($title){$this->title=$title;}

	function showLI($menuSelected){
		if($this->getVisible()==0)return '';
		if($this->varGet==$menuSelected){
                                $this->addCssLI('actif');
				$this->addCssSP('actif');
                                $this->addCssA ('actif');
                                }
		$cssLI=' class="'.$this->cssLI.'"';
		$cssSP =' class="'.$this->cssSP .'"';
		$cssA =' class="'.$this->cssA .'"';
		$a="<a $cssA  href='{$this->getHref}' title='{$this->title}' {$this->inlineA} >{$this->titre}</a>";
		return "<li$cssLI {$this->inlineLI} ><span $cssSP>$a</span></li>";
		}
	}
	
class menuStylisee
	{
	public $cssMenu='';
	public $li=array();

	function __construct($cssMenu){$this->cssMenu=$cssMenu;}

	function __toString()
		{
		$out='<b>'. __CLASS__.'</b><ul>';
		$out.='<ul>';
		foreach ($this->li as $key => $li){$out.='<li>'.$li.'</li>';}
		unset($li);
		$out.='</ul>';
		return $out.'</ul>';
		}
	
	/* -- affiche tous les li -- */
	function show($menuSelected=NULL)
		{
		$liBuild='';
		foreach ($this->li as $key => $li)$liBuild.=$li->showLI($menuSelected);
		unset($key);unset($li);
		return "<div class='{$this->cssMenu}'><ul>$liBuild</ul></div><div class='clear'></div>\n";
		}



	// = FONCTIONS D'APELLE PROTEGES au TABLEAU li  = //

	/*---
	function addLI($index,$titre,$get=NULL,$url=NULL,$classe=NULL)
		index: nom de l'index dans le tableau des onglets. Attention: Remplace l'existant
		titre: 
		get:
		url:
		classe:class css qui sera ajouter lors de l'affichage  creation 
	---*/

	function addLI($index,$titre,$get=NULL,$url=NULL,$css=NULL,$inlineLI=NULL,$inlineA=NULL){
		$this->li[$index]=new menuStylisee_li($titre,$get,$url,$css,$inlineLI,$inlineA);
		}

	function addCssLI($index,$css){if(isset($this->li[$index]))$this->li[$index]->addCssLI($css);}
	function addCssSP($index,$css){if(isset($this->li[$index]))$this->li[$index]->addCssSP($css);}
	function addCssA ($index,$css){if(isset($this->li[$index]))$this->li[$index]->addCssA ($css);}

	function setTitle($title){if(isset($this->li[$index]))$this->li[$index]->setTitle($title);}

	} //menuStylisee
// -- *********************************************************************** -- //
// -- Fin de la lib -- //
// -- *********************************************************************** -- //
$gestLib->setEtat('menuStylisee',LEGRAL_LIBETAT::LOADED);
$gestLib->end('menuStylisee');

?>
