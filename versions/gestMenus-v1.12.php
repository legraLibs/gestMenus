<?php
/*!******************************************************************
fichier: gestMenus.php
version : 0.1.10
auteur : Pascal TOLEDO
date de creation:31 mai 2014
date de modification: 08 juillet  2014
source: http://www.legral.fr/intersites/lib/perso/php/gestMenu
gitorious:

depend de:
	* gesLib-0.1.php
description:

tutoriel:
// ordre de surcharge1 est surcharge par 2 qui est surcharge par 3 etc
// 1- session	// $pageLast_session
// 2- cookie	// $pageLast_cook
// 3- get	// $pageLast_Get

// reparation de fortune apres avoir tout cassé avec git et la branche ajoutMachin

***********************************************************************/
$gestLib->loadLib('gestMenus',__FILE__,'0.1',"gestionnaire de menus");
//	$gestLib->lib['gestMenus']->setErr(LEGRALERR::DEBUG);

/***********************************************************************
Convertie les caracteres speciaux en leurs equivalents
***********************************************************************/
function convertAccent($text){
	//$ext=htmlentities($text,ENT_NOQUOTES,'UTF-8');$text=htmlspecialchars_decode($text);return $text;
	$carSpec=array('é','è','à','â','ô','î','ç');
	$htmlSpec=array('&eacute;','&egrave;','&agrave;','&acirc;','&ocirc;','&icirc;','&ccedil;');
	return str_replace($carSpec,$htmlSpec,$text);
	}


/***********************************************************************
class gestMeta
Sauvegarde dans une pile des menus successives avec leur page respective.
***********************************************************************/
class gestMetas
	{

//	public  $titlePrefixe=''; // prefixea ajoute au meta html title
	private $meta=array();    // meta connu:  leur code sera ajouter. Seule la valeur doit etre renseigner (tittle,refresh) eg: title="mon titre" -> <title>mon titre</title>
	private $metaFull=array();	// metas non connu: le code sera ecrit tel quel dans le <head>

	function __construct(){
		$this->meta['title']='';	// doit etre defini pour afficher la balise meta (meme si vide)
		$this->meta['unique']='<meta name="test" content="pour test only"/>';
		}
	
	function shows(){return $this->showMeta().$this->showMetaFull();}

	function getMeta($meta){return (iseet($this->meta[$meta])?$this->meta[$meta]:'');}
        function setMeta($meta,$val){$this->meta[$meta]=$val;}

        function showMetas($prefixe=[]){
		$titlePrefixe='';
		if(is_array($prefixe)){
			//echo "prefixe defini\n";
			//echo gestLib_inspect('prefixe',$prefixe);
			if(isset($prefixe['title'])){$titlePrefixe=$prefixe['title'];/*echo "titlePrefixe modifier\n";*/}
			}
                $out='<!-- metas specifique -->';
		foreach($this->meta as $k =>$v){
                        switch($k){
                                case 'refresh':$out.="";break;
                                case 'title':$out.="<title>{$titlePrefixe}$v</title>";//echo "htmltitle afficher\n";break;
                                case 'favicon':$out.='<link rel="icon" href="'.$v.'" />';break;
                                default:$out.=$this->meta[$k]."\n";
                                }
                        }
                return $out;
                }

        function getMetaFull($meta){return(iseet($this->metaFull[$meta])?$this->metaFull[$meta]:'');}
        function setMetaFull($meta,$val){$this->metaFull[$meta]=$val;}
        function showMetaFull(){$out='<!-- metas complet -->';foreach($this->metaFull as $v)$out.=$v;  return $out;}
	}


/***********************************************************************
class gestAriane
Sauvegarde dans une pile des menus successives avec leur page respective.
***********************************************************************/
class gestAriane
	{
	private $pileNb=0;
	private $lastMenu='';
	private $lastPage='';
	public  $arianeGet='';	// affiche l'ariane sous forme menu1=page1&amp;menu2=page2&amp;etc
	public  $pile=array();
	// -- -- //
	function __construct(){}

	function __toString(){
		$out='';
		for($i=0;$i<$this->pileNb;$i++)$out.='wmenu:'.$this->pile[$i]['menu'].'; page:'.$this->pile[$i]['page'].'<br>';
		return $out;
		}
	function addMenu($menu,$page)
		{
		if(!$menu){return -1;}
		$this->pile[$this->pileNb]['menu']=$menu;
		$this->pile[$this->pileNb]['page']=$page;
		$this->lastMenu=$menu;
		$this->lastPage=$page;
		$this->arianeGet.="&amp;menu=$page";
		$this->pileNb++;
		}

	public function getlastMenu(){return $this->lastMenu;}
	public function getlastPage(){return $this->lastPage;}


	// -- renvoie la chaine ariane -- // 
	function showAriane(){
		$href='?';
		$out='';
		for($m=0;$m<$this->pileNb;$m++){
			$menu=$this->pile[$m]['menu'];
			$page=$this->pile[$m]['page'];
			$href.="$menu=$page&amp;";
			$out.='-&gt;';
			$out.="<a href='$href'title='page:$page'>$menu</a>";
			}
		return "$out\n";
		}

	// -- retourne ?menu1=page1&amp;menu2=page2&amp;menumenuLvl=pagemenuLvl&amp;-- //
	function getHref($menuLvl){
		global $gestLib;
		
		//echo $gestLib->debugShowVar('gestMenus',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$menuLvl',$menuLvl);
		$href='?';
                for($m=0;$m<$menuLvl;$m++)
                        {
                        $menu=(isset($this->pile[$m]['menu']))?$this->pile[$m]['menu']:'';
                        $page=(isset($this->pile[$m]['page']))?'='.$this->pile[$m]['page']:'';
                        $href.="$menu$page&amp;";
			//echo $gestLib->debugShowVar('gestMenus',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'$href',$href);

                        }
                return "$href";
		}

	}

// - *********************************************************************** - //
// - gestionnaire d'appel des pages - //
// - *********************************************************************** - //
class gestCallPage
	{
	private $cssLI='';      // liste des classes css qui sera ajoute au code <li> (eg:'ms')
        private $cssA='';       // liste des classes css qui sera ajoute au code <a>  (eg:'actif')

	private $attr=array();	// parametres de la page
					// URL:
					// visible:
					// menuTitre: titre qui sera afficher
	private $get=array();	// sert de code dasn une url (eg: dans ?menu1=page2, get=menu1)
	public  $metas=NULL;	// gestionnaire de metas voir gestMetas

	function __construct($url)
		{
		$this->attr['URL']=$url;
		$this->attr['visible']=1;	// par defaut: visible
		$this->metas=new gestMetas();
		}

        function __toString(){
                return __CLASS__.gestLib_inspect($this).'<br>';
                }

	function getAttr($attrNom){return(isset($this->attr[$attrNom])?$this->attr[$attrNom]:NULL);}
	function setAttr($attrNom,$attrVal){if($attrNom){$this->attr[$attrNom]=$attrVal;}}

	function getGet($getNom){return(isset($this->get[$getNom])?$this->get[$getNom]:NULL);}
	function setGet($getNom,$getVal){if($getNom){$this->get[$getNom]=$getVal;}}

	//-  manipulation des attributs de la page - //

	// -- getMenuTitle: renvoie $menuTitle si non definie renvoie le titre le definit le plus proche -- //
	function getMenuTitle(){
		return (isset($this->attr['menuTitle']))?$this->attr['menuTitle']
		     : (isset($this->attr['menuTitre']))?$this->attr['menuTitre']
		     : (isset($this->attr['titre']    ))?$this->attr['titre']
		     : $this->attr['URL'];
		     ;
		}

	 //-  manipulation des classes des sous composants du template menuStylisee:onglet<li><a> - //
	function getCssLI(){return $this->cssLI;} function setCssLI($classes){$this->cssLI=$classes;} function addCssLI($classes){$this->cssLI.=" $classes";}
	function getCssSP(){return $this->cssSP;} function setCssSP($classes){$this->cssSP=$classes;} function addCssSP($classes){$this->cssSP.=" $classes";}
	function getCssA (){return $this->cssA;}  function setCssA ($classes){$this->cssA =$classes;} function addCssA ($classes){$this->cssA .=" $classes";}
	}
	
// -- *********************************************************************** -- //
// -- gestion d'un menu-- //
// -- un menu gere des pages -- // 
// -- *********************************************************************** -- //
class gestMenu
	{
	public  $gestNom=NULL;		// nom du gestionnaire
	public  $pageLast=NULL;
	private $pageDefaut=NULL;	// page par defaut
	private $URL_defaut=NULL;
	public  $pageNomActuelle=NULL;
	public  $varGet=NULL;           // nom de la var utiliser dans get ex: ?page=

	public  $template='onglets';	// genre du template 
	public  $classes='ms';		// classes de base pour tout le menu (LIs)

	public  $metas=NULL;		// gestionnaire de metas pour le menus lui-meme
	public  $callPages=array();		// pages (attributs)page[page]->attr


	function __construct($nom,$pageDefaut='',$URL_defaut='',$varGet=NULL)
		{
		$this->gestNom=$nom;
		$this->varGet=$varGet?$varGet:$nom;
		$this->pageDefaut=$pageDefaut;		$this->URL_defaut=$URL_defaut;

		$this->pageNomActuelle=$this->pageDefaut;	$this->pageLast=$this->pageDefaut;

		if($pageDefaut!=''){
			$this->addCallPage($pageDefaut,$URL_defaut);	// supprimer car appelais un
			$this->setPageLast();
			$this->setPageNomActuelle();
			}
		}	

	// - renvoyer les donnees de l'object si demande d'affichage - //
        function __toString(){
		global $gestLib;
                $out=__CLASS__;
		//foreach($this as $key => $value)  $out.=$gestLib->inspect($key,$value);
		$out.=$gestLib->inspect($this);
                return $out;
                }

	//-  addClasses: ajoute une ou plusieurs classe a integré a toutes les pages  - //
	public function addClasses($classes){$this->classes.=" $classes";}

	//-  addClassesInPage: ajoute une ou plusieurs classe a integré a une page  - //
	public function addClassesInPage($page,$classes){if(isset($this->$pages[$page]))$this->$pages[$page]->addClasses($classes);}


	public function getPageLast(){
		return $this->pageLast;}

	public function setPageLast($newPageLast=NULL){
		global $gestLib;
		if(isset($_COOKIE[$this->gestNom.'_pageNomActuelle'])){$this->pageLast=$_COOKIE[$this->gestNom.'_pageNomActuelle'];}
		if($newPageLast){$this->pageLast=$newPageLast;}
		setcookie($this->gestNom.'_pageLast',$this->pageLast,(time()+3600*24*365));

		// - rajouter ici le code js a ecrire dans le head - //

		}

	public function addCallPage($pageNom,$url){
		$this->callPages[$pageNom]=new gestCallPage($url);}

	public function getPageNomActuelle(){
		return $this->pageNomActuelle;}

	public function setPageNomActuelle($page=NULL){
		global $gestLib;
		if($this->pageLast){$this->pageNomActuelle=$this->pageLast;}
		if(isset($_GET [$this->varGet])){$this->pageNomActuelle=$_GET [$this->varGet];}
		if(isset($_POST[$this->varGet])){$this->pageNomActuelle=$_POST[$this->varGet];}
		if($page){$this->pageNomActuelle=$page;}
		setcookie($this->gestNom.'_pageNomActuelle', $this->pageNomActuelle,(time()+3600*24*365));
		}
	public function getURL($page=NULL)
		{
		$page=$page?$page:$this->pageNomActuelle;
		if(@$this->callPages[$page]){$url=$this->callPages[$page]->getAttr('URL');return $url?$url:$this->URL_defaut;}//accee direct 'pages[$page]->getAttr' oblige!
		return NULL;
		}

	//-- Ouvre un fichier et renvoie le texte avec les accents convertit -- //
	private function parserPage($url){
		$out='';if($fd=@fopen($url,'r')){while(!feof($fd)){$buffer=fgets($fd,4096);$out.=$buffer;}fclose($fd);}return convertAccent($out);}

	public function incPage($page=NULL){
		global $gestLib;
		$page=$page?$page:$this->pageNomActuelle;
		$url=$this->getURL($page);
		if($url==NULL){$url=$this->URL_defaut;}
		if(file_exists($url))
			{
			echo $gestLib->debugShow('gestMenus',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'le fichier existe -&gt; inclusion('.$url.')');
			}
		elseif($this->getURL(404)){$url=$this->getURL(404);echo '404 appeller!';}
		else{$url=$this->URL_defaut;}
		if(substr($url,-4)=='.php'){include $url;}else{echo $this->parserPage($url);}//si .php inclure sinon parser pour accent
		}
	
	public function showGet($page=NULL){
		return(isset($this->callPages[$page]))?$this->callPages[$page]->showGet():NULL;}

	// --- Accee aux attr des pages --- //
	public function setAttr($page,$attrNom,$attrVal){
		if(isset($this->callPages[$page]) AND $attrNom){$this->callPages[$page]->setAttr($attrNom,$attrVal);}}

	public function getAttr($page,$attrNom){
		if($attrNom=='URL'){return $this->getURL($page);}
		return isset($this->callPages[$page])?$this->callPages[$page]->getAttr($attrNom):NULL;
		}

	public function setGet($page,$getNom,$getVal){
		if(isset($this->callPages[$page]) AND $getNom){$this->callPages[$page]->setGet($getNom,$getVal);}}

	public function getGet($page,$getNom){
	     return isset($this->callPages[$page])?$this->callPages[$page]->getAttr($attrNom):NULL;}

	public function toArray($attr=NULL,$attrVal=NULL)
		{$out=array();
		if(!$attr)foreach($this->callPages as $key => $value){$out[]=$key;}
		elseif(!$attrVal)//
			{
			foreach($this->calPages as $key => $value){if ($value->getAttr($attr)){$out[]=$key;}}
			}
		else	{
			foreach($this->callPages as $key => $value){if ($value->getAttr($attr)==$attrVal){$out[]=$key;}}
			}
		return $out;
		}
/*
	// --- renvoie TRUE si la page a l'attribue ssMenu --- //
	function isPagemenu($page){
		if( (isset($this->calPages[$page])) AND  ($this->callPages[$page]->getAttr('ssMenu')===1) )return TRUE;
		return FALSE;
		}
*/

	// -- manipuler les meta des pages-- //
	function meta_shows($page){if(isset($this->callPages[$page]))return $this->callPages[$page]->meta->shows();}

        function getMeta($page,$meta)         {if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->getMeta($meta);}
        function setMeta($page,$meta,$val)    {if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->setMeta($meta,$val);}

        function showMeta($page)              {if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->showMeta();}
  
        function getMetaFull($page,$meta)     {if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->getMetaFull($meta);}

        function setMetaFull($page,$meta,$val){if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->setMetaFull($meta,$val);}

        function showMetaFull($page)          {if(isset($this->callPages[$page]))  return $this->callPages[$page]->metas->showMeta();}


	// -- manipuler les css des pages-- //
	function getCssLI($page)              {if(isset($this->callPages[$page]))  return $this->callPages[$page]->getCssLI();}
	function setCssLI($page,$css)         {if(isset($this->callPages[$page])) return $this->callPages[$page]->setCssLI($css);}
	function addCssLI($page,$css)         {if(isset($this->callPages[$page])) return $this->callPages[$page]->addCssLI($css);}	

	function getCssA ($page)              {if(isset($this->callPages[$page]))  return $this->callPages[$page]->getCssA ();}
	function setCssA ($page,$css)         {if(isset($this->callPages[$page])) return $this->callPages[$page]->setCssA ($css);}
	function addCssA ($page,$css)         {if(isset($this->callPages[$page])) return $this->callPages[$page]->addCssA ($css);}

	// -- return le menu selon le tempate -- //
	function showMenu($gestMenus){
		$out='';
		switch ($this->template)
			{
/*
			case'selectOption':
				$out='<select>';
				foreach ($gestMenus->menus['tutoriels']->toArray('menu',1) as $key => $pageIndex)
					{
					$selected=' ';
					if($pageIndex==$gestMenus->menus['tutoriels']->getPageNomActuelle()){$gestMenus->menus['tutoriels']->setAttr($pageIndex,'classe','UL_classMenu_actif');$selected=' selected="selected" ';}
					$out.="<option $selected class='{$gestMenus->menus['tutoriels']->getAttr($pageIndex,'classe')}'><a href='{$gestMenus->menus['tutoriels']->varGet}=$pageIndex'>{$$gestMenus->menus['tutoriels']->getAttr($pageIndex,'menuTitre')}</a></option>";
					}
				$out.='</select>';
				break;
	
			case'UL':
				$out='<ul>';
				foreach ($tutoriels_gestPage->toArray('menu',1) as $key => $pageIndex)
					{
					if($pageIndex==$tutoriels_gestPage->getPageNomActuelle()){$tutoriels_gestPage->setAttr($pageIndex,'classe','UL_classMenu_actif');}
					$out.="<li class='{$tutoriels_gestPage->getAttr($pageIndex,'classe')}'><a href='{$tutoriels_gestPage->varGet}=$pageIndex'>{$tutoriels_gestPage->getAttr($pageIndex,'menuTitre')}</a></li>";
					}
				$out.='</ul>';
				break;

*/

			case'css':
				break;
			case'ms':case'ms:onglets':
			default:
				$onglets=new menuStylisee($this->classes);
				foreach ($this->toArray('visible',1) as $key => $pageIndex){
					$titre=$this->getAttr($pageIndex,'menuTitre');
					$getHref=$gestMenus->ariane->getHref($gestMenus->menuLvl);
					$getHref.=$this->varGet.'='.$pageIndex;

					$onglets->addLi($pageIndex,$titre,$getHref);
						$onglets->li[$pageIndex]->setVarGet($pageIndex);
//						$onglets->li[$pageIndex]->setMeta
						$onglets->li[$pageIndex]->addCssLI($this->callPages[$pageIndex]->getCssLI());   // ajouter les classes affectes au <li>
						$onglets->li[$pageIndex]->addCssA ($this->callPages[$pageIndex]->getCssA ());   // ajouter les classes affectes au <a>

					}// foreach
				$out.=$onglets->show($this->getPageNomActuelle());
//				$out.=' getPageNomActuelle:'.$this->getPageNomActuelle();
//				$out.=' varGet:'.$this->varGet;
				break;
			}
		return $out;
		} // showMenu()
	} // class gestMenu


// -- *********************************************************************** -- //
// -- metaclass de gestion DES menuS -- //
// gere l'historique global, les meta html , l'ajout des menus via le parse du get
// -- *********************************************************************** -- //
class gestMenus{
	public  $menuDefaut='';          // menu qui sera appelle en 1er (ou si aucun menu est appelle)

	public  $menuLvl=1;		// nombre de niveau de menu appellé (pour les li)
        public  $template='ms:onglets';	// style du template par defaut: onglet,select,UL

	private $_pageSelect='';	// page (qui n'est pas un menu) trouvé et sauvé par build; à usage interne et intermediare.
	private $_buildData='';		// contenu du menu contruit

	private $metas;                 // se connecte sur un gestionnaire des metas html (et title) d'une page (n'instancie aucun gestionnaire)
	public  $metasPrefixes=array();  // prefixe ajoute au meta html title (pour gestMetas)

	public  $ariane=NULL;           // ariane des pages actives
	public  $menus=array();         // tableau de gestionnaire de menu (gestMenu)
//	public  $pages=NULL;            // future tableau de gestionnnaire de page (a creer)

        function __construct($menuDefaut)
                {
		$this->ariane=new gestAriane();
		$this->metas=NULL;		// n'intancie pas le gestionnaire mais se connecte sur un autre
		$this->menuDefaut=$menuDefaut;  // menu qui sera en premier
                }

        // - renvoyer les donnees de l'object si demande d'affichage - //
        function __toString(){
                global $gestLib;
                $out=__CLASS__.'<br>';
		foreach($this as $key => $value)  $out.=$gestLib->inspect($key,$value);
		//$out.=$gestLib->inspect($this);	// 500:internal server error // NE PAS SUPPRIMER CETTE LIGNE
                return $out;
                }

	// - renvoie le titre de la derniere page d'ariane- //
	function getLastTitre(){
		$menu=$this->ariane->getlastMenu();
		$page=$this->ariane->getlastPage();
		return isset($this->menus[$menu]->callPages[$page])?$this->menus[$menu]->callPages[$page]->getAttr('titre'):'';
		}

	function getLastMenuTitle(){
                $menu=$this->ariane->getlastMenu();
                $page=$this->ariane->getlastPage();
		return isset($this->menus[$menu]->callPages[$page])?$this->menus[$menu]->callPages[$page]->getMenuTitle():'';
                }


	// - ajoute un menu dasn le tableau des menus - //
	// - $menuTitre,$titre,$varGet='',$pageDefaut NON IMPLEMENTER  - //
	function addMenu($menuNom,$menuTitre='',$titre='',$varGet='',$pageDefaut=''){
		 //                                   ($nom,$pageDefaut,$URL_defaut,$varGet=NULL)
		$m=$this->menus[$menuNom]=new gestMenu($menuNom,$pageDefaut);
		if($varGet!=='')$m->varGet=$varGet;
		return $m;
		}

	// -- genere les menus et sauve la page selectionnée-- //
	function build($showMeta=1){
		global $gestLib;
		$qs=$_SERVER['QUERY_STRING'];$str=array();parse_str($qs,$str);
		$menuOut='';
		$m=$p='';
		foreach($str as $m => $p){

			// **** reorganisation debut  **** //

			if(!isset($this->menus[$m]))continue; // ce psp n'est pas un menu il ne regarde donc pas ce programme

			// - gestion de la partie menu du psp - //

			// -- on ne prend que son 1er appel -- //
			// le menu est appelle 2 fois: une fois par le menu et une fois par la page du menu precedent
		        if($m!=$this->ariane->getLastMenu())
		                {
				$this->menuLvl++;

				// -- maj d'ariane -- //
				$this->ariane->addMenu($this->menus[$m]->gestNom,$this->menus[$m]->getPageNomActuelle());

				// -- construction et lecture de ce menu -- //
				$menuOut.=$this->menus[$m]->showMenu($this);

				// -- initialisation du gestionnaire terminal des metas -- //
				$this->metas=$this->menus[$m]->metas;
				}

			// - gestion de la partie page du psp - //
			// -- la page est il un menu? (la page a appelle un autre menu) -- //
			if(isset($this->menus[$p]))
		                {       
				//echo "la page $p est un menu -> on ajoute ce menu<br>";

				// -- maj d'ariane -- //
	 			$this->ariane->addMenu($this->menus[$p]->gestNom,$this->menus[$p]->getPageNomActuelle());

				// -- construction et lecture de ce menu -- //
				$menuOut.=$this->menus[$p]->showMenu($this);

				// -- initialisation du gestionnaire terminal des  metas -- //
				$this->metas=$this->menus[$p]->metas;
				}       
			else{// echo "$p n'est pas un menu)<br>";
				// -- est-ce une page valide ? -- //
				if(isset($this->menus[$m]->callPages[$p])){

					// -- initialisation du gestionnaire final des  metas -- //
					$this->metas=$this->menus[$m]->callPages[$p]->metas;

					// --- mise a jours  de metaTitle avec le titre de la page --- //
					$pageTitre=$this->menus[$m]->callPages[$p]->getAttr('titre');
					//echo gestLib_inspect('getTitre',$pageTitre).'<br>';
					$this->metas->setMeta('title',$pageTitre);
					}
				}	
			} // foreach

		// -- sauvegarde des valeurs-- // 
		$this->_buildData="$menuOut\n";
		$this->_pageSelect=$m;

		// -- affichage des metas -- //
		if($showMeta){
			// recupere les metas de la page selectionnée par le dernier menu
			echo '<!-- meta specifique -->';
			if($this->metas instanceof gestMetas){
				echo $this->metas->showMetas($this->metasPrefixes);
				}
			else echo '<!-- aucun gestionnaire de meta instancie car rien a heriter -->';
			}
		}
		
	// - affiche le menu - //
	// - menuFisrt: menu qui doit etre affiché quand rien n'est demander - //
	function show($menuFisrt=NULL){
		if($menuFisrt==NULL)$menuFisrt=$this->menuDefaut;

		// - afficher le 1er menu si aucun menu definit dans le get - //
		if(($this->menuLvl==1) and (isset($this->menus[$menuFisrt])) ) echo $out=$this->menus[$menuFisrt]->showMenu($this);

		// - afficher le menu construit avec build() - //
		echo $this->_buildData;

		// - afficher la page actif du dernier menu - //
	        if(isset($this->menus[$this->_pageSelect]))$this->menus[$this->_pageSelect]->incPage();
		}
	}

// -- *********************************************************************** -- //
// -- Fin de la lib -- //
// -- *********************************************************************** -- //
$gestLib->setEtat('gestMenus',LEGRAL_LIBETAT::LOADED);
//$gestMenus=new gestMenus(); // creation d'une instance 

$gestLib->end('gestMenus');
?>
