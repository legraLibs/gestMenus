<?php
error_reporting(E_ALL);
//error_reporting(NULL);

define('VERSIONSTATIQUE',0);
if (defined('VERSIONSTATIQUE')){
	if(VERSIONSTATIQUE == 1){
		error_reporting(NULL);
		//display_errors(0);
		}
	}

// - docs communs- //
define('DOCUMENT_ROOT',$_SERVER['DOCUMENT_ROOT'].'/');
define('INTERSITES_ROOT','./');
define('_0PAGES_ROOT','/www/0pages/');
define('PAGES_ROOT','./pages/');
define('_0TEXTES_ROOT',DOCUMENT_ROOT.'0textes/');
define('_0ARTICLES_ROOT',DOCUMENT_ROOT.'0articles/');

// - docs specifiques auprojet - //
define('INDEX_ROOT','./');
define('MENU_ROOT','./menus/');
define('PAGESLOCALES_ROOT','./pagesLocales/');

//  === bufferisation ====  //
ob_start();

//  ==== session ==== //
session_name('legral_session');session_start();

//include (INTERSITES_ROOT.'lib/legral/php/intersites/intersites-3.0.php');


//  === gestionnaire de librairies ====  //
$lib='gestLib';
include (INTERSITES_ROOT."lib/legral/php/$lib/$lib.php");
//      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);
/* --- appelle d'une version fixe --- //
include (INTERSITES_ROOT."lib/legral/php/$lib/$lib-v1.0.0.php");
      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);
 */

//include (INTERSITES_ROOT.'lib/legral/php/intersites/intersites-3.0.php');

$lib='menuStylisee';
//include("./lib/legral/php/$lib/$lib-v0.1.php");
//include("./lib/legral/php/$lib/$lib.php");
include("./$lib.php");
//      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);

$lib='gestMenus';
//include("./lib/legral/php/$lib/$lib.php");
//include("./versions/$lib-v1.14.0.php");
include("./$lib.php");
//      $gestLib->libs[$lib]->setErr(LEGRALERR::DEBUG);
unset($lib);


//  ==== autorisation local ==== //


//  ==== Gestion des menus et pages  ==== //
$gestMenus=new gestMenus('gestMenusProject');
$gestMenus->metasPrefixes['title']='lib - gestMenus';
include(MENU_ROOT.'menus-systeme.php');
include(MENU_ROOT.'menus-gestMenus.php');


//  ==== html ==== //
?><!DOCTYPE html><html lang="fr">
<head>
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<meta http-equiv="Content-Style-Type" content="text/css">
<meta http-equiv="Content-Script-Type" content="text/javascript">
<meta name="robots" content="index,follow">
<meta name="author" content="Pascal TOLEDO">
<meta name="description" content="">
<meta name="keywords" content="">
<meta name="generator" content="vim">
<meta name="identifier-url" content="http://legral.fr">
<meta name="date-creation-yyyymmdd" content="20140804">
<meta name="date-update-yyyymmdd" content="201400804">
<meta name="reply-to" content="pascal.toledo@legral.fr">
<meta name="revisit-after" content="10 days">
<meta name="category" content="">
<meta name="publisher" content="legral.fr">
<meta name="copyright" content="pascal TOLEDO">
<?php 
$gestMenus->build();
//define('ARIANE', $gestMenus->ariane->getHref(NULL,''));//''=pas de PSPstart // neccesssite build()

//echo gestLib_inspect('$gestMenus->ariane->pile',$gestMenus->ariane->pile);
//echo gestLib_inspect('$gestMenus->ariane->pile',$gestMenus->ariane->pile[0]);
?>
<!-- scripts -->
<script src="./locales/scripts.min.js"> </script>

<!-- styles -->
<link rel="stylesheet" href="./styles/styles.css" media="all" />
</head>

<body>
<div id="page">

<!-- header -->
<div id="header">
<div id="headerGauche"><!--gauche--></div>
<h1><a href="http://legral.fr/">legralNet</a> : <a href="?<?php echo $gestMenus->menuDefaut?>">lib - gestMenus</a></h1>

<div id="headerDroit"><a href="?about=accueil">&agrave; propos de...</a></div>
</div><!-- header -->

<!-- menu + ariane + contenu page + ariane -->
<?php
echo '<div class="ariane">'.$gestMenus->ariane->showAriane().'</div>'."\n";

// - affiche les menus contruit en commencant par le menu indiquee par le constructeur. Inclu la page appellee - //
echo $gestMenus->show();

echo $gestLib->tableau();


// -- ariane: affichage-- //
echo "\n".'<div class="ariane">'.$gestMenus->ariane->showAriane().'</div>'."\n";
?><!-- menu + ariane + contenu page + ariane : FIN -->


<!-- footer -->
<!-- footer gauche-->
<div id="footer">
<div id="footerGauche"><?php if(defined('VERSIONSTATIQUE')){
        if(VERSIONSTATIQUE == 1)echo 'version statique(g&eacute;n&eacute;r&eacute;e le: '.date('d/m/Y').')';
        }
?></div><!-- footer gauche: fin-->

<!-- footer centre -->
<?php echo $gestMenus->getLastTitre().'<br>';   //afficher le titre de la page?>
<span class="licence">
<a rel="license" href="http://creativecommons.org/licenses/by/4.0/">
<img alt="Licence Creative Commons" src="./styles/img/licenceCCBY-88x31.png" /></a><br />Mise &agrave; disposition selon les termes de la <a rel="license" href="http://creativecommons.org/licenses/by/4.0/">licence Creative Commons Attribution 4.0 International</a>.<br />
<a xmlns:dct="http://purl.org/dc/terms/" href="http://legral.fr" rel="dct:source">http://legral.fr</a>.</span>
<!-- footer centre: fin -->

<!-- footer droit -->
<div id="footerDroit"></div>
<!-- footer droit: fin -->


</div><!-- footer -->

</div><!-- //page -->

<?php include('./piwik.php');?>
</body></html>
