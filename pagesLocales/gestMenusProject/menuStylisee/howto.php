<?php

$menu1=new menuStylisee('ms');

// -- ajout des entrees du menu-- //

//addLi     ($index,	$titre,										$get,		$urlL(fichier de destination)){
//$menu1->addLi('li1',	'sans get, sans destination');

//$menu1->addLi('li11',   'avec get, sans url', 'page=page11');

// -- ce menu n'a pas vraiment de raison d'etre! -- //
//$menu1->addLi('page1',	'Appel page1.html sans get'	, NULL		,'./page1.html');

$menu1->addLi('li3','avec css (dossierIcone classe3)', '?page=li3');
$menu1->addCSSLI('li3','dossierIcone classe3');

$menu1->addLi('left','avec css (dossierIconeLeft)', '?page=left');
$menu1->addCSSA('left','dossierIconeLeft');

$menu1->addLi('right','avec css (dossierIconeRight)', '?page=right');
$menu1->addCSSA('right','dossierIconeRight');


$menu1->addLi('li4',     'sans get, sans url, onclickLI/A ');
$menu1->addInlineLI('li4','onclick="alert(\'clic sur LI\');"');
$menu1->addInlineA ('li4','onclick="alert(\'clic sur A\');"');

$menu1->addLi('li5',	'avec url sur _blank'	,'?page=li5','./pagesLocales/menuStylisee/page5.html');
//$menu1->addInlineA ('li5','target="_blank"');

$menu1->addCSSA('li5','dossier1');

$menu1->addLi('na','url inexistante', NULL,'./inexistant.php');
?>


<h1 class="h1">lib: menuStylisee</h1>

<p>Cette librairie contruit un menu selon le template appellé et creait une url specifique a chaque entree de menu
<div class="noteclassic">note: Il n'appelle pas les pages associées au menu. Il faut utiliser pour cela la librairie gestMenu ou gestPage. Les 2 utilise menuStylisee par defaut.</div>
</p>

<p>instanciation d'un menu:<br>
<pre class="coding_code">
$menu=new menuStylisee('ms');<br>
ms: nom de la classe appellé par le menu
</pre>
<h2>menu1</h2>
<p>On affiche le menu</p>
<?php 
$page=(isset($_GET['page']))?$_GET['page']:'';

// - affiche le menu avec l'entree du menu correspondant a $page en etat selectionne - //
echo $menu1->show($page);

?>
Menu avec la class 'ms'.<br>
Nous revoila!<br>

<div onclick="displaySwitch('menu1Inspect')" style="cursor:help">inspection du menu1</div>
<div id="menu1Inspect" style="display:none"><?php echo gestLib_inspect("menu1",$menu1);?>
</div>

