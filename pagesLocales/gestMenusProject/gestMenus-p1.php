<?php function showCmd($cmd){	echo "<pre class='coding_file'>$cmd</pre>";}?>

<style>
h2.h2{
margin-top:1em;
}
</style>

<h1 class="h1">gestLib version PHP</h1>
<h2 class="h2">inclure gestLib:</h2>
<pre class='coding_file'>include './gestLib.php'</pre>

<h2 class="h2">Ajouter une librairie:</h2>
<pre class='coding_file'>$gestLib->loadLib('gestLib-Test',__FILE__,'0.1','fichier de test pour gestLib');</pre>

<?php $gestLib->loadLib('gestLib-Test',__FILE__,'0.1','fichier de test pour gestLib');?>



<h1 class="h1">gestLib_inspect: Inspecter une variable.</h1>
<p>La fonction gestLib_inspect est touours afficher quelque soi le niveau d'erreur.</p>
<?php
echo '<h2 class="h2">NULL:</h2>';
$var='NULL';
$cmd='$var='.$var.';echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",NULL);
$var='null';
$cmd='$var='.$var.';echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",null);


echo '<h2 class="h2">boolean:</h2>';
$var='TRUE';
$cmd='$var='.$var.';echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",TRUE);
$var='FALSE';
$cmd='$var='.$var.';echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",FALSE);


echo '<h2 class="h2">chaine de caractere:</h2>';
$var='chaine';
$cmd='$var="'.$var.'";echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",$var);

echo '<h2 class="h2">chaine vide \'\':</h2>';
$var='';
$cmd='$var="'.$var.'";echo gestLib_inspect("var","");';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",$var);


echo '<h2 class="h2">numerique:</h2>';
$var=0;
$cmd='$var='.$var.';echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",$var);

$var=12345;
$cmd='$var='.$var.';echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",$var);

$var=12345.6789;
$cmd='$var='.$var.';echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",$var);


echo '<h2 class="h2">Tableau:</h2>';
$var='array("key1" => "val1","key2" => 2,"key3" => "val3")';
$_var=array("key1" => "val1","key2" => 2,"key3" => "val3");
$cmd='$var='.$var.';echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",$_var);

// - inspecter une class - //
echo '<h2 class="h2">class vide:</h2>';
$var='$var=new classVide();';
class classVide{};
$_var=new classVide();;
$cmd='class classVide{};$var=new classVide(); echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",$_var);
echo 'if(!get_class_methods($var))';


echo '<h2 class="h2">class:</h2>';
class classA{public $str="moiiiii";public $num=554545;public $tab=NULL; function __construct(){}};
$_var=new classA();
$cmd='class classA{public $str="moiiiii";public $num=554545;public $tab=NULL; function __construct(){}  };$classA=new classA($tab);';
$cmd.=' echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",$_var);

echo '<h2 class="h2">class avec une methode __toString:</h2>';

class classtoString
	{
	public $titre='class Test avec fonction __toString()';
	function __construct(){}
	function __toString(){return 'je suis de la classe test';}
	}

$_var=new classtoString();
$cmd='class testAvec__toString'
	.'{'
	.'public $titre="class Test avec fonction __toString()";'
	.'function __construct(){}'
	.'function __toString(){return "je suis de la classe test";}'
	.'}'
	.'$var=new classtoString()';
$cmd.=' echo gestLib_inspect("var",$var);';
echo "<pre class='coding_file'>$cmd</pre>";
echo gestLib_inspect("var",$_var);
?>
(ca ne change rien)

<hr>

<h2 class="h2">Ajouter une librairie:</h2>
<pre class='coding_file'>$gestLib->loadLib('gestLib-Test',__FILE__,'0.1','fichier de test pour gestLib');</pre>

<?php $gestLib->loadLib('gestLib-Test',__FILE__,'0.1','fichier de test pour gestLib');?>


<h2 class="h2">niveau d'erreur:</h2>
<ul>
	<li>LEGRALERR::ALWAYS: Toujours afficher les erreurs</li>
	<li>LEGRALERR::NOERROR: Ne jamais afficher les erreurs</li>
	<li>LEGRALERR::CRITIQUE: Afficher uniquement les erreurs CRITIQUES</li>
	<li>LEGRALERR::DEBUG: Afficher les erreurs DEBUG et CRITIQUE</li>
	<li>LEGRALERR::WARNING: Afficher les erreurs WARNING, DEBUG et CRITIQUE</li>
	<li>LEGRALERR::INFO: Afficher les erreurs INDFO,WARNING, DEBUG et CRITIQUE</li>
	<li>LEGRALERR::ALL: Afficher toutes les erreurs</li>
</ul>

<h2 class="h2">Définir le niveau d'erreur:</h2>
<pre class="coding_file">$gestLib->libs['gestLib-Test']->setErr(LEGRALERR::DEBUG);</pre>
<?php $gestLib->libs['gestLib-Test']->setErr(LEGRALERR::DEBUG); ?>

<h2 class="h2">Récuperer le niveau d'erreur:</h2>
<pre class="coding_file">echo $gestLib->getLibError('gestLib-Test');</pre>
<?php echo $gestLib->getLibError('gestLib-Test');?>



<h2 class="h2">$gestLib->debugShow: Afficher un texte localiser:</h2>
<pre class="coding_file">echo $gestLib->debugShow('gestLib-Test',LEGRALERR::ALWAYS,__LINE__,__FUNCTION__,'Marque 1');</pre>
<?php echo $gestLib->debugShow('gestLib-Test',LEGRALERR::ALWAYS,__LINE__,__FUNCTION__,'Marque 1');?>

<h2 class="h2">$gestLib->debugShowVar: Afficher la valeur d'une variable:</h2>

<pre class="coding_file">$gestLib->debugShowVar('NomDeLaLib',NIVEAU_DERREUR(ex:LEGRALERR::CRITIQUE),__LINE__,__FUNCTION__,'nom de la var',$var);</pre><br>
<pre class="coding_file">
$var='variable';
echo echo $gestLib->debugShowVar('gestLib-Test',LEGRALERR::CRITIQUE,__LINE__,__FUNCTION__,'var',$var);</pre>
<?php
$var='variable';
echo $gestLib->debugShowVar('gestLib-Test',LEGRALERR::CRITIQUE,__LINE__,__FUNCTION__,'var',$var);
?>

<pre class="coding_file">
function fonction_test(){
	global $gestLib;
	$var="variable est a l'interieur d'une fonction.";
	echo $gestLib->debugShowVar('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'var',$var);
}
fonction_test();
</pre>
<?php
function fonction_test(){
	global $gestLib;
	$var="variable est a l'interieur d'une fonction.";
	echo $gestLib->debugShowVar('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'var',$var);
}
fonction_test();
?>

<h2 class="h2">format d'affichage</h2>
<ul>
	<li>'','nobr',NULL:aucun ajout (inline)</li>
	<li>'br': ajoute &lt;br&gt; (defaut)</li>
	<li>'ln':ajoute "\n"</li>
	<li>'div':'p' met le texte dans une &lt;div&gt; ou un &lt;p&gt; (possibilité de préciser une class avec le parametre suivant)</li>
</ul>
<pre class="coding_file">$gestLib->debugShow('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'format:aucun','')</pre>
<?php echo $gestLib->debugShow('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'format:NULL',''); ?>
--PAS DE SAUT DE LIGNE--

<pre class="coding_file">$gestLib->debugShow('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'format:br','br')</pre>
<?php echo $gestLib->debugShow('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'format:br','br'); ?>

<pre class="coding_file">$gestLib->debugShow('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'format:ln','ln')</pre>
<?php echo $gestLib->debugShow('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'format:ln','ln'); ?>
--SAUT DE LIGNE: \n--

<pre class="coding_file">$gestLib->debugShow('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'format:div','div')</pre>
<?php echo $gestLib->debugShow('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'format:div','div'); ?>

<pre class="coding_file">$gestLib->debugShow('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'format:div avec classe bold','div')</pre>
<?php echo $gestLib->debugShow('gestLib-Test',LEGRALERR::DEBUG,__LINE__,__FUNCTION__,'format:div avec classe bold!','div','bold'); ?>


<hr />
<?php 

// - inspecter une variable - //

class testAvecString
	{
	public $titre='class Test avec fonction __toString()';
	function __construct(){}
	function __toString(){return 'je suis de la classe test';}
	}
class testSansString
        {
        public $titre='class Test sans fonction __toString()';
	function __construct(){}
        // function __toString(){return 'je suis de la classe test';} // NE PAS DECOMMENTER, NE PAS ACTIVER
        }


/*
echo '<h3>inspecter une classe sans __toString()</h3>';
$testSans=new testSansString();echo $gestLib->inspect('testSans',$testSans).'<br>';

echo '<h3>Inspecter une classe avec __toString()</h3>';
$testAvec=new testAvecString();echo $gestLib->inspect('testAvec',$testAvec);
 */

// - indiquer a gestLib que la lib est charger - //
$gestLib->end('gestLib-Test');
?>

<h2 class="h2">Afficher un tableau recapitulatif des librairies chargées</h2>
<pre class="coding_file">echo $gestLib->libTableau();</pre>
<?php echo $gestLib->libTableau(); ?>


