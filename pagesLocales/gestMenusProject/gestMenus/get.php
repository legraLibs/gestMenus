<?php
global $gestMenus;
?>
<?php

$cmd='';
//$cmd.='
//define('ARIANE', $gestMenus->ariane->getHref(NULL,''));//''=pas de PSPstart
?>

<h1 class="h1">Manipulation du get</h1>


<h2 class="h2">Intégré le fil d'ariane dans un &lt;a&gt;</h2>
<pre class="coding_code">&lt;a href="?&lt;?php echo ARIANE?&gt;"&gt;   &lt;?php echo ARIANE?&gt;   &lt;/a&gt;</pre>
<a href="?<?php echo ARIANE?>"><?php echo ARIANE?></a>

<h2 class="h2">Intégré le fil d'ariane dans un &lt;a&gt; avec des parametres additionnels</h2>
<pre class="coding_code">&lt;a href="?arg1=1&amp;arg2='deux'&amp;&lt;?php echo ARIANE?&gt;"&gt; arg1=1&amp;arg2="deux"&amp;&lt;?php echo ARIANE?&gt;   &lt;/a&gt;</pre>
<a href="?arg1=1&arg2='deux'&<?php echo ARIANE?>">arg1=1&amp;arg2="deux"&amp;<?php echo ARIANE?></a>

