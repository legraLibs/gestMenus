<h1 class="h1">gestLib version js</h1>

<!-- consoles -->
<div id="gestLibConsole" class=""></div>
<div id="libTestConsole" class=""></div>



<script>
var obj1={
	"a":"obj_a"
	,"v":1
}

// - activation de la console - //
gestLib.setConsole('gestLib');			// console de type DIV
//gestLib.setConsole('gestLib','TEXTAREA');	// console de type TEXTAREA


gestLib.write({"lib":"gestLib","ALL":"ecrit dans la console gestLib quel que soit le genre"});

gestLib.write({"lib":"gestLib","TEXTAREA":"texte afficher dans la console gestLib si le genre est TEXTAREA"});

gestLib.write({"lib":"gestLib","DIV":"texte afficher dans la console gestLib si le genre est DIV"});


gestLib.write({"lib":"gestLib","ALL":"test de la fonction inspect"});

gestLib.inspect({"lib":"gestLib","varNom":"null","varPtr":null});
gestLib.inspect({"lib":"gestLib","varNom":"undefined","varPtr":undefined});
gestLib.inspect({"lib":"gestLib","varNom":"textBrut","varPtr":"texte brute"});
gestLib.inspect({"lib":"gestLib","varNom":"1","varPtr":1});
gestLib.inspect({"lib":"gestLib","varNom":"obj1","varPtr":obj1});

gestLib.write({"lib":"gestLib","ALL":"test de la fonction inspectAll(): cette fonction bug!"});

gestLib.write({"lib":"gestLib","ALL": gestLib.inspectAll({"lib":"gestLib","varNom":"null","varPtr":null})  });
gestLib.write({"lib":"gestLib","ALL": gestLib.inspectAll({"lib":"gestLib","varNom":"undefined","varPtr":undefined})  });
gestLib.write({"lib":"gestLib","ALL": gestLib.inspectAll({"lib":"gestLib","varNom":"textBrut","varPtr":"texte brute"})  });
gestLib.write({"lib":"gestLib","ALL": gestLib.inspectAll({"lib":"gestLib","varNom":"1","varPtr":1})  });
 
gestLib.write({"lib":"gestLib","ALL": gestLib.inspect({"lib":"gestLib","varNom":"obj1","varPtr":obj1})  });

gestLib.write({"lib":"gestLib","ALL":"utilisation des instances"});
gestLib.write({"lib":"gestLib","ALL":"Cela eu servit a quelque chose,mais a quoi?"});


// - ajout d'une lib - //
gestLib.loadLib({
	nom:'libTest'
	//,idHTML:'gestLib'
	,ver:'0.0.0'
	,description:'lib vide pour test'
	,isConsole:0	// si 1: construit automatiquement une console du genre DIVla balise html doit etre construite avant cette appelle
	,isVisible:1	// fonction desactiver
	,url:'http://exemple.com'
});
gestLib.instanceAdd('libTest',gestLib.libs['libTest']);
gestLib.setConsole('libTest','TEXTAREA');	// console de type TEXTAREA
gestLib.write({"lib":"libTest","TEXTAREA":"\n\n"});
gestLib.write({"lib":"libTest","TEXTAREA":"//le code precedent est ok?"});
gestLib.write({"lib":"libTest","TEXTAREA":"gestLib.write({'lib':'gestLib','ALL':'code ok'});"});




document.write(gestLib.tableau());
</script>


